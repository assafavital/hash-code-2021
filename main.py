import random
import sys
from typing import List, Dict

import numpy as np

from input_reader import StatementInput, Street

statement_input: StatementInput = StatementInput(sys.argv[1])


def get_number_of_cars_by_street() -> Dict[str, int]:
    res = {}
    for car in statement_input.carsList:
        for street_ahlan in car.streets:
            if street_ahlan in res:
                res[street_ahlan] += 1
            else:
                res[street_ahlan] = 1
    return res


def get_streets_to_ignore():
    cars_by_street = get_number_of_cars_by_street()
    cars_by_street_np: np.ndarray = np.array(list(cars_by_street.values()))
    p = np.percentile(cars_by_street_np, 30)

    return [key for key in cars_by_street if cars_by_street[key] < p]


def get_scheduled_streets(intersection: int) -> List[Street]:
    ignore = get_streets_to_ignore()
    return [street_to_ignore for street_to_ignore in statement_input.streetList if street_to_ignore.end == intersection
            and street_to_ignore not in ignore]


def get_incoming_streets(intersection: int) -> List[Street]:
    return [street for street in statement_input.streetList if street.end == intersection]


if __name__ == '__main__':
    with open('result_d.txt', 'w') as f:
        f.write(str(len(statement_input.intersections)) + '\n')
        for intersection in statement_input.intersections:
            f.write(str(intersection) + '\n')
            possible_streets = get_scheduled_streets(intersection)
            f.write(str(len(possible_streets)) + '\n')
            for street in possible_streets:
                street: Street
                f.write('{} {}'.format(street.name, random.randint(1, 42)) + '\n')

    x = 1
