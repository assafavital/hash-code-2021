from typing import List


class Street:
    start: int
    end: int
    name: str
    time: int

    def __init__(self, row: str):
        params: List[str] = row.split(' ')
        self.start = int(params[0])
        self.end = int(params[1])
        self.name = params[2]
        self.time = int(params[3])


class Intersection:
    intersection_id: int

    def __init__(self, row: str):
        params: List[str] = row.split(' ')
        self.intersection_id = int(params[0])




class Car:
    numberOfStreets: int
    streets: List[str] = []

    def __init__(self, row: str):
        params: List[str] = row.split(' ')
        self.numberOfStreets = int(params[0])
        for p in params[1:]:
            self.streets.append(p)


class StatementInput:
    simulationDuration: int
    intersections: List[int]
    streets: int
    cars: int
    bonusPoints: int
    streetList: List[Street] = []
    carsList: List[Car] = []

    def __init__(self, filename: str):
        with open(filename) as f:
            content: List[str] = [line.strip() for line in f.readlines()]
            first_line_params: List[str] = content[0].split(' ')
            self.simulationDuration = int(first_line_params[0])
            self.streets = int(first_line_params[2])
            self.cars = int(first_line_params[3])
            self.bonusPoints = int(first_line_params[4])
            for row in content[1:self.streets + 1]:
                self.streetList.append(Street(row))
            for row in content[self.streets + 1:]:
                self.carsList.append(Car(row))

            starts = [street.start for street in self.streetList]
            ends = [street.end for street in self.streetList]
            self.intersections = set(ends + starts)